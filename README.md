
# CLI Book Management Tool

This is a Command-Line Interface (CLI) tool for managing a collection of books. The application allows users to add, view, update, delete, and list books stored in a JSON file. The application is built using Python and consists of two classes: Book and BookManager. The tool takes data from a JSON file, books.json, and allows the user to interact with the data through a menu of options.

## Installation

1. Clone the repository to your local machine:

```bash
git clone https://github.com/your_username/cli-book-management-tool.git
```
2. Change directory to the project directory:

```bash
cd cli-book-management-tool
```

3. Install Python 3.x on your computer, if not already installed. Install the dependencies by running the following command:

```bash
pip install -r requirements.txt
```

4. Run the main.py file to start the application:

```bash
python main.py
```

## Usage

Once you have installed and started the tool, you will be presented with a menu of following options. Choose an option by entering the corresponding number and pressing Enter.

```markdown
-------------------------------------------
        Book Management Tool
-------------------------------------------
        *** Menu ***
-------------------------------------------
1. Add Book
2. List Books
3. Update Book
4. Delete Book
5. List Authors
6. View Book Details
7. Quit
-------------------------------------------

```

Follow the prompts to enter the required information for each option. Once you have completed the action, you will be returned to the main menu.

### 1. Add Book
Selecting this option allows you to add a new book to the collection. You will be prompted to enter the book's title, author, publisher and publication year.

### 2. List Books
Selecting this option allows you to list all the books in the collection.

### 3. Update Book
Selecting this option allows you to update the details of an existing book in the collection. You will be prompted to enter the book ID of the book you want to update, and then you will be prompted to enter the new details of the book.

### 4. Delete Book
Selecting this option allows you to delete a book from the collection. You will be prompted to enter the book ID of the book you want to delete.

### 5. List Authors
Selecting this option allows you to list all the authors of the books in the collection.

### 6. View Book Details
Selecting this option allows you to view the details of a specific book in the collection. You will be prompted to enter the book ID of the book you want to view.

### 7. Quit
Selecting this option allows you to exit the application.

## JSON File Format

The application uses a JSON file named books.json to store the book data. The file contains an array of objects, with each object representing a book. Each book object has the following properties:

- id: The unique identifier of the book (integer)
- title: The title of the book (string)
- author: The name of the author of the book (string)
- publisher: The name of the publisher of the book (string)
- year_published: The publication year of the book (string in the format 'YYYY')

Here's an example of what the books.json file might look like:
```json
[
  {
    "id": 1,
    "title": "The Great Gatsby",
    "author": "F. Scott Fitzgerald",
    "publisher": "Amazon",
    "year_published": "1925"
  },
  {
    "id": 2,
    "title": "To Kill a Mockingbird",
    "author": "Harper Lee",
    "publisher": "Amazon",
    "year_published": "1960"
  }
]

```
## Classes
## Book

The Book class represents a book object and contains the book's attributes such as `title`, `author`, `publisher`, `year_published`, and `id`. The class is used to create instances of book objects with the specified attributes.

### Constructor
```python
def __init__(self, title, author, publisher, year_published):
    """
    Initialize Book object with attributes title, author, publisher, and year_published
    get the next available ID for the book

    :param title: str, title of the book
    :param author: str, author of the book
    :param publisher: str, publisher of the book
    :param year_published: str, year published of the book
    """

```
- The constructor initializes a `Book` object with the following parameters:
    - `title`: a string that represents the title of the book.
    - `author`: a string that represents the author of the book.
    - `publisher`: a string that represents the publisher of the book.
    - `year_published`: a string that represents the year the book was published.
- The constructor also gets the next available ID for the book using the `get_next_id` method.

### Methods
#### get_next_id()
```python
@staticmethod
def get_next_id():
    """
    Static method to get the next available ID for the book from the books.json file
    """

```
- The `get_next_id` method is a static method used to get the next available ID for the book.
- It reads data from a JSON file called `books.json`.
- If the `books.json` file does not exist, the method returns 1 as the next available ID.
- If the `books.json` file exists but there are no books in the data, the method returns 1 as the next available ID.
- If there are books in the data, the method returns the last ID in the data plus 1.

### Attributes

#### id
- An integer that represents the ID of the book.
- The ID is automatically generated by the `get_next_id` method.
#### title
- A string that represents the title of the book.
#### author
- A string that represents the author of the book.
#### publisher
- A string that represents the publisher of the book.
#### year_published
- A string that represents the year the book was published.

### Usage
To use the `Book` class, you can create a `Book` object by passing in the required parameters as shown below:

```python
book = Book("The Great Gatsby", "F. Scott Fitzgerald", "Amazon", "1925")

```
The `id` attribute is generated automatically when the Book object is created.

## BookManager

The `BookManager` class is a class that helps to manage books data in a JSON file. The class provides methods for adding books, listing all books and authors, updating a book's details, viewing particular book details and deleting a book. It also has a helper function for getting valid year input from the user.

### Initialization
The `BookManager` class can be initialized with a path to a JSON file that will contain the book data.

```python
from BookManager import BookManager

book_manager = BookManager("books.json")

```
If the JSON file does not exist at the given path, a new file will be created with an empty list of books.

### Methods

#### Add a Book

```python
book_manager.add_book()

```
This method prompts the user to input information about a new book and adds it to the book collection in the JSON file

#### List Books

```python
book_manager.list_books(page_size=10)

```
This method displays the list of books in pages, with each page containing `page_size` number of books. The default `page_size` is 10.

#### Update a Book

```python
book_manager.update_book(id)

```
This method prompts the user to enter new details for a book with a given ID, and updates the details in the book collection in the JSON file.

#### Delete a Book

```python
book_manager.delete_book(id)


```
This method removes a book with a given ID from the book collection in the JSON file.

#### View Book Details

```python
book_manager.view_book(id)


```
This method shows book details with a given ID from the book collection in the JSON file.

#### List Authors

```python
book_manager.list_authors()

```
This method displays the list of unique authors from the book collection in the JSON file.

### Helper Function
#### Get a Valid Year

```python
year = book_manager.valid_year()

```
This method prompts the user to input a year and returns the year as a string. It validates that the year is within the accepted range (between 1900 and 2023).


## License
This project is licensed under the terms of the MIT license. See the `LICENSE` file for more information.

## Contributing
Contributions to this project are welcome. To contribute, fork the repository, make changes, and submit a pull request.






