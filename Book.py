import json
import os


class Book:
    def __init__(self, title, author, publisher, year_published):
        """
        Initialize Book object with attributes title, author, publisher, and year_published
        get the next available ID for the book

        :param title: str, title of the book
        :param author: str, author of the book
        :param publisher: str, publisher of the book
        :param year_published: str, year published of the book
        """
        # Get the next available ID for the book
        self.id = self.get_next_id()
        self.title = title
        self.author = author
        self.publisher = publisher
        self.year_published = year_published

    @staticmethod
    def get_next_id():
        """
        Static method to get the next available ID for the book from the books.json file
        """
        filename = 'books.json'

        # Check if books.json does not exist, return 1 as the next ID
        if not os.path.exists(filename):
            return 1

        # Read data from books.json
        with open(filename, 'r') as f:
            data = json.load(f)
            # If there are no books in the data, return 1 as the next ID
            if len(data) == 0:
                return 1
            else:
                # Otherwise, return the last ID in the data plus 1
                last_id = data[-1]['id']
                return last_id + 1
