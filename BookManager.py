import json
import os
from Book import Book


class BookManager:
    """
    Initialize BookManager object with json_file attribute

    :param json_file: str, path to JSON file that contains the books data
    """

    def __init__(self, json_file):
        """
        :param json_file: str, path to JSON file that contains the books data
        """
        self.json_file = json_file
        # If json_file does not exist, create an empty file
        if not os.path.exists(self.json_file):
            with open(self.json_file, 'w') as f:
                json.dump([], f)

    def valid_year(self):
        """
        Helper function to get a valid year from user input

        :return: str, valid year of the book
        """
        flag = True
        while (flag):
            year_published = input("Enter year published: ")
            try:
                year = int(year_published)
                # Validate the year is within the accepted range
                if year < 1900 or year > 2023:
                    print("-------------------------------------------")
                    print("Invalid year. Please enter a year between 1900 and 2023.")
                    print("-------------------------------------------")
                    continue
                else:
                    flag = False
                    break
            except ValueError:
                print("-------------------------------------------")
                print("Invalid input. Please enter a valid published year.")
                print("-------------------------------------------")
                continue
        return str(year)

    def add_book(self):
        """
        Add a book to the books data
        """
        # Open the JSON file for reading and writing
        with open(self.json_file, 'r+') as f:
            data = json.load(f)
            # prompt user for information about the new book
            title = input("Enter title of book: ")
            author = input("Enter author of book: ")
            publisher = input("Enter publisher of book: ")
            year_published = self.valid_year()
            book = Book(title, author, publisher, year_published)
            # append the new book to the data
            data.append({'id': book.id, 'title': book.title, 'author': book.author,
                        'publisher': book.publisher, 'year_published': book.year_published})
            f.seek(0)
            open(self.json_file, 'w')
            json.dump(data, f)

    def list_books(self, page_size=10):
        """
        List all books in the books data in pages

        :param page_size: int, number of books to display per page (default = 10)
        """
        # Open the JSON file that contains the books data
        with open(self.json_file, 'r') as f:
            # Load the books data from the file
            data = json.load(f)
            # Calculate the number of pages based on the number of books and the page size
            num_pages = len(data) // page_size + 1
            # Display the books in pages
            print("\t\tPage 1")
            print("-------------------------------------------")
            for i in range(num_pages):
                start = i * page_size
                end = min((i + 1) * page_size, len(data))
                print('\n'.join(
                    [f"{book['id']}. {book['title']}" for idx, book in enumerate(data[start:end])]))
                # If all the books have been displayed, break the loop
                if end == len(data):
                    break
                # If there are more pages, prompt the user to enter any key to continue
                if i != num_pages - 1:
                    print("-------------------------------------------")
                    input("Press any key for next page (ex. 'n'): ")
                    print("-------------------------------------------")
                    print(f"\t\tPage {i+2}")
                    print("-------------------------------------------")

    def update_book(self, id):
        """
        Update the details of a book in the books data

        :param id: int, ID of the book to update
        :return: bool, True if book was updated, False otherwise
        """
        # Open the JSON file that contains the books data
        with open(self.json_file, 'r+') as f:
            # Load the books data from the file
            data = json.load(f)
            # Loop through the books data and find the book with the given ID
            for book in data:
                if book['id'] == id:
                    # Display the details of the current book and prompt the user to enter the new details
                    print(
                        f"\n----- Current Book Details -----\nTitle: {book['title']}\nAuthor: {book['author']}\nPublisher: {book['publisher']}\nPublish Year: {book['year_published']}\n-------------------------------------------\n")
                    book['title'] = input("Enter new title: ")
                    book['author'] = input("Enter new author name: ")
                    book['publisher'] = input("Enter new publisher name: ")
                    book['year_published'] = self.valid_year()
                    # Move the file pointer to the beginning of the file and write the updated data to the file
                    f.seek(0)
                    open(self.json_file, 'w')
                    json.dump(data, f)
                    # Return True to indicate that the book was updated
                    return True
            # If the book was not found, return False
            return False

    def delete_book(self, id):
        """
        Deletes a book with the given ID from the data.

        :param id: int, ID of the book to delete
        :return: bool, True if book was deleted, False otherwise
        """
        # Open the JSON file for reading and writing
        with open(self.json_file, 'r+') as f:
            data = json.load(f)
            for idx, book in enumerate(data):
                # If the ID of the current book matches the ID we want to delete
                if book['id'] == id:
                    # Remove the book from the data
                    del data[idx]
                    f.seek(0)
                    open(self.json_file, 'w')
                    # Write the modified data back to the file
                    json.dump(data, f)
                    print("-------------------------------------------")
                    print(f"Recently deleted book: {book['title']}")
                    # Return True to indicate that a book was deleted
                    return True
            # If the book wasn't found, return False to indicate that no books were deleted
            return False

    def view_book(self, id):
        """
        Displays the details of a book with the given ID.

        :param id: int, The ID of the book to be viewed.
        :return: bool, True if the book was found and its details were displayed, False otherwise.
        """
        # Open the JSON file for reading
        with open(self.json_file, 'r') as f:
            data = json.load(f)
            for book in data:
                # If the ID of the current book matches the ID we want to view
                if book['id'] == id:
                    print(
                        f"\n----- Book Details -----\nTitle: {book['title']}\nAuthor: {book['author']}\nPublisher: {book['publisher']}\nPublish Year: {book['year_published']}\n")
                    # Return True to indicate that a book was found and viewed
                    return True
            # If the book wasn't found, return False to indicate that no books were viewed
            return False

    def list_authors(self):
        """
        Lists the names of all authors of the books in the library.
        """
        # Open the JSON file for reading
        with open(self.json_file, 'r') as f:
            data = json.load(f)
            # Create a set of unique author names from the data
            authors = set([book['author'] for book in data])
            # Print the author names, separated by newlines
            print('\n'.join(authors))
