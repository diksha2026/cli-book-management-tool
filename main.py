"""
CLI Book Management tool
"""
from BookManager import BookManager

if __name__ == '__main__':
    """
    This code is the main module of a book management tool. It creates a BookManager object using data from a JSON file, and then presents a menu of options for the user to choose from.
    The code continuously runs until the user chooses to quit.
    """
    # Create a BookManager object using data from a JSON file
    bm = BookManager('books.json')

    # Continuously present a menu of options for the user to choose from
    while True:
        # Display the menu options
        print("-------------------------------------------")
        print("\n\tBook Management Tool\n")
        print("-------------------------------------------")
        print("\t*** Menu ***")
        print("-------------------------------------------")
        print("1. Add Book")
        print("2. List Books")
        print("3. Update Book")
        print("4. Delete Book")
        print("5. List Authors")
        print("6. View Book Details")
        print("7. Quit")
        print("-------------------------------------------")

        # Get the user's choice
        choice = input("Enter your choice (1 to 7): ")

        print("-------------------------------------------")

        # Handle the user's choice
        if choice == '1':
            # Add a new book
            print("Add Book")
            print("-------------------------------------------")
            bm.add_book()
            print("-------------------------------------------")
            print("Book added successfully")

        elif choice == '2':
            # List all the books
            print("List Books")
            print("-------------------------------------------")
            bm.list_books()

        elif choice == '3':
            # Update an existing book
            print("Update Book")
            print("-------------------------------------------")
            id = int(input("Enter id of book to update: "))
            if bm.update_book(id):
                print("-------------------------------------------")
                print("Book updated successfully")
            else:
                print("-------------------------------------------")
                print("Book not found")

        elif choice == '4':
            # Delete an existing book
            print("Delete Book")
            print("-------------------------------------------")
            id = int(input("Enter id of book to delete: "))
            if bm.delete_book(id):
                print("-------------------------------------------")
                print("Book deleted successfully")

            else:
                print("-------------------------------------------")
                print("Book not found")

        elif choice == '5':
            # List all the authors
            print("List Authors")
            print("-------------------------------------------")
            bm.list_authors()

        elif choice == '6':
            # View details of a specific book
            print("View Book Details")
            print("-------------------------------------------")
            id = int(input("Enter id of book to view details: "))
            if bm.view_book(id):
                print("-------------------------------------------")
                print("Book fetched successfully")
            else:
                print("-------------------------------------------")
                print("Book not found")

        elif choice == '7':
            # Quit the program
            print("Quit")
            print("-------------------------------------------")
            print("Thank you! Visit again!")
            print("-------------------------------------------")
            break

        else:
            # Handle invalid choices
            print("Invalid choice! Please choose correct menu id (1 to 7)")
